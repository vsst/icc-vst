** Problem **

What's your proudest achievement ?

** Answer **

> While working at Bitdefender, I was working on a system was designed to have 1 central server (which was also a database and a ftp server) and was serving big files to 8 other computers. Of course, because it was a single central server, trying to serve all those files, the more requests it had, the slower the speed was. (the slowest came under 1MB/s)

> Everyone tried to solve this problem by throwing more hardware at the server, but, the problem was the design. So, I organized a meeting where I presented another option: using torrents to transfer files. This way, every computer will also be serving files instead of just download. This idea was met with skepticism, and no one wanted to help me implement it.

> But I had a hunch that this will work really well and I did a prototype the next day, and showed how powerful this was. After people saw the prototype, I was given green light to implement it from scratch, which I did.This cut downloading a file from 30-40 minutes to under 5 minutes.

> This taught me that some of the simplest solutions are the best, but not everyone thinks this way (and also that you need to bring numbers and prototypes to the table, to convince people easily :D).

*Hope you enjoyed my little story*
