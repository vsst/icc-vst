import argparse
import json as JSON
import requests
import sys
from math import sin, cos, acos, radians

DEFAULTS = {
    'location': [53.3381985, -6.2592576],
    'radius': 100.0
}

class Distance:
    EARTH_RADIUS = float("6378.1370")

    def between(self, origin, destination):
        if not isinstance(origin, Location) or not isinstance(destination, Location):
            raise TypeError("origin and destination must be of type Location")

        distance = Distance.EARTH_RADIUS * acos( \
            sin(radians(origin.latitude)) * sin(radians(destination.latitude)) + \
            cos(radians(origin.latitude)) * cos(radians(destination.latitude)) * \
            cos(abs(radians(origin.longitude) - radians(destination.longitude))) \
        )

        return distance


class Location:
    def __init__(self, latitude, longitude):
        self.latitude, self.longitude = float(latitude), float(longitude)

    def distance_to(self, other_location):
        if not isinstance(other_location, Location):
            raise TypeError("other_location must be of type Location")

        return Distance().between(self, other_location)

    def __str__(self):
        return "Location [latitude: {}, longitude: {}]".format(self.latitude, self.longitude)

    def __repr__(self):
        return self.__str__()

class Customer:
    def __init__(self, user_id, name, latitude, longitude):
        self.user_id, self.name = int(user_id), str(name)
        self.location = Location(latitude, longitude)

    @staticmethod
    def from_json(json):
        json_dict = JSON.loads(json)
        return Customer(json_dict["user_id"], json_dict["name"],
                        json_dict["latitude"], json_dict["longitude"])

    def __lt__(self, other):
        return self.user_id < other.user_id

    def __gt__(self, other):
        return self.user_id > other.user_id

    def __str__(self):
        return "Customer [user_id: {}, name: {}, location: {}]".format(self.user_id, self.name, self.location)

    def __repr__(self):
        return self.__str__()


def parse_arguments(args):
    parser = argparse.ArgumentParser(description="Invite customers in a radius to the party")
    parser.add_argument('--location', '-l', type=float, nargs=2, default=DEFAULTS['location'], help="Party location coordinates")
    parser.add_argument('--radius', '-r', type=float, default=DEFAULTS['radius'], help="Radius")
    parser.add_argument('--json', '-j', action="store_true", default=False, help="output as json")

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--url', '-u', help="Url that contains customer data")
    group.add_argument('--path', '-p', help="Path to file that contains customer data")

    return parser.parse_args(args)

def download_customers(url):
    response = requests.get(url)
    lines = response.text.split('\n')

    customers = []
    for line in lines:
        customers.append(Customer.from_json(line))

    return customers

def load_customers(path):
    customers = []
    with open(path, 'r') as f:
        for line in f.readlines():
            customers.append(Customer.from_json(line))

    return customers

def filter_customers(customers, party_location, radius):
    filtered_customers = filter(lambda customer: party_location.distance_to(customer.location) < radius, customers)
    return list(filtered_customers)

def print_customers_json(customers):
    json_to_dump = [{"user_id": customer.user_id, "name": customer.name} for customer in customers]

    print(JSON.dumps(json_to_dump, indent=2))

def print_customers(customers, party_location, radius):
    print("Customers closer than {} km to be invited to {}"
          .format(radius, party_location))

    for customer in customers:
        print("User Id: {}, Name: {}, Distance: {}"
              .format(customer.user_id, customer.name,
                      party_location.distance_to(customer.location)))

if __name__ == "__main__":
    args = parse_arguments(sys.argv[1:])
    party_location = Location(*args.location)
    radius = args.radius

    customers = []
    if args.url is not None:
        customers = download_customers(args.url)
    elif args.path is not None:
        customers = load_customers(args.path)
    else:
        raise Exception("--path and --url are both absent, 1 should be present")

    customers_to_invite = filter_customers(customers, party_location, radius)
    customers_to_invite.sort()
    if args.json:
        print_customers_json(customers_to_invite)
    else:
        print_customers(customers_to_invite, party_location, radius)


