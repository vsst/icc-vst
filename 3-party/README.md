** Problem **

Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by user id (ascending)

** Commands **

To run the script (some examples):
```
$ python invite.py --radius 100 --url https://gist.githubusercontent.com/brianw/19896c50afa89ad4dec3/raw/6c11047887a03483c50017c1d451667fd62a53ca/gistfile1.txt
$ python invite.py --radius 100 --path customers.json
$ python invite.py --json --radius 100 --path customers.json

$ python invite.py -h                             
usage: invite.py [-h] [--location LOCATION LOCATION] [--radius RADIUS]
                 [--json] (--url URL | --path PATH)

Invite customers in a radius to the party

optional arguments:
  -h, --help            show this help message and exit
  --location LOCATION LOCATION, -l LOCATION LOCATION
                        Party location coordinates
  --radius RADIUS, -r RADIUS
                        Radius
  --json, -j            output as json
  --url URL, -u URL     Url that contains customer data
  --path PATH, -p PATH  Path to file that contains customer data

```

To run tests:
```
$ nosetests -v -s # use --with-watcher to rerun tests on change
```

** Implementation **

The implementation is straightforward. You can either feed the customers (punny ^_^) through a file on disk or through an URL.
