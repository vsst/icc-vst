from nose.tools import assert_almost_equal, raises, assert_true, assert_equals, assert_false
from invite import Distance, Location, Customer, parse_arguments, DEFAULTS

class TestBetween:
    def setup(self):
        self.obj_under_test = Distance()

    def teardown(self):
        pass

    def test_correct_distance_calculation(self):
        origin = Location(53.3381985, -6.2592576)
        destination = Location(52.833502, -8.522366)

        expected_distance = 161.39734312
        actual_distance = self.obj_under_test.between(origin, destination)

        assert_almost_equal(actual_distance, expected_distance)

    @raises(TypeError)
    def test_raised_exception_on_origin_type_wrong(self):
        origin = ""
        destination = Location(52.833502, -8.522366)

        self.obj_under_test.between(origin, destination)

    @raises(TypeError)
    def test_raised_exception_on_destination_type_wrong(self):
        origin = Location(53.3381985, -6.2592576)
        destination = {}

        self.obj_under_test.between(origin, destination)

class TestLocation:
    def setup(self):
        pass

    def teardown(self):
        pass

    def test_latitude_and_longitude_set(self):
        latitude = 123.456789
        longitude = 987.654321

        location = Location(latitude, longitude)

        assert_almost_equal(location.latitude, latitude)
        assert_almost_equal(location.longitude, longitude)

    def test_distance_to(self):
        origin_latitude = 123.456789
        origin_longitude = 987.654321
        origin_location = Location(origin_latitude, origin_longitude)

        dest_latitude = 23.456789
        dest_longitude = 87.654321
        dest_location = Location(dest_latitude, dest_longitude)

        distance_to_distance = origin_location.distance_to(dest_location)
        distance_class_distance = Distance().between(origin_location, dest_location)

        assert_almost_equal(distance_to_distance, distance_class_distance)

    @raises(TypeError)
    def test_distance_to_raises_error_on_wrong_type(self):
        Location(123.234, 345.456).distance_to("")


class TestCustomer:
    def setup(self):
        pass

    def teardown(self):
        pass


    def test_customer_sorted_by_user_id(self):
        list_of_customers = [
            Customer(987, 'name', 543, 454),
            Customer(56, 'name', 123, 234),
            Customer(3, 'name', 123, 234)
        ]

        list_of_customers.sort()

        assert_true(all([list_of_customers[i].user_id < list_of_customers[i+1].user_id for i in range(len(list_of_customers) - 1)]))

    def test_customer_from_json(self):
        json = '{"user_id":"420",\
                "name":"random",\
                "latitude":"123.456",\
                "longitude":"987.654"}'

        customer = Customer.from_json(json)

        assert_equals(customer.user_id, 420)
        assert_equals(customer.name, "random")
        assert_almost_equal(customer.location.latitude, 123.456)
        assert_almost_equal(customer.location.longitude, 987.654)


    @raises(KeyError)
    def test_customer_from_json_raises_exception_on_invalid_json(self):
        json = '{"smth":"abc"}'

        Customer.from_json(json)


class TestArgumentParser:
    def setup(self):
        pass

    def teardown(self):
        pass

    @raises(SystemExit)
    def test_raise_exception_on_url_and_path_missing(self):
        parse_arguments([])

    @raises(SystemExit)
    def test_raise_exception_if_url_and_path_both_set(self):
        parse_arguments(["--url", "some_url", "--path", "some/path"])

    def test_doesnt_raise_exception_if_url_set(self):
        args = parse_arguments(["--url", "some_url"])
        assert_equals(args.url, "some_url")

    def test_doesnt_raise_exception_if_path_set(self):
        args = parse_arguments(["--path", "some/path"])
        assert_equals(args.path, "some/path")

    def test_default_settings(self):
        args = parse_arguments(["--url", "some_url"])
        assert_equals(args.url, "some_url")
        assert_equals(args.location, DEFAULTS["location"])
        assert_equals(args.radius, DEFAULTS["radius"])
        assert_false(args.json)

    def test_custom_settings(self):
        args = parse_arguments([
            "--path", "some/path",
             "--location", "123.234", "234.345",
             "--radius", "420.3",
             "--json"
         ])

        assert_equals(args.path, "some/path")
        assert_equals(args.location, [123.234, 234.345])
        assert_equals(args.radius, 420.3)
        assert_true(args.json)
