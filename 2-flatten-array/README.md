** Problem **

Write a function that will flatten an array of arbitrary nested array of integers

** Commands **

To run the script
```
$ python flatten.py
```

To run tests:
```
$ nosetests -v -s # use --with-watcher to rerun tests on change
```

** Implementations **

There are 4 implementations:

* 2 iterative implementations (1 using stacks)
* 2 recursive implementations (1 using python generators)

** The one using python generators has to be transformed to list before printed (using list())


**Speed comparison of different implementations**

```
$ ./compare.sh

# Flatten Iterative:		10000 loops, best of 3: 26.7 usec per loop

# Flatten Iterative with Stack:	10000 loops, best of 3: 27.6 usec per loop
  
# Flatten Recursive:		10000 loops, best of 3: 35.4 usec per loop
  
# Flatten Generator:		100000 loops, best of 3: 5.42 usec per loop
  
# Flatten list(Generator):	10000 loops, best of 3: 23.9 usec per loop
```
