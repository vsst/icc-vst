from nose.tools import raises, assert_equals, assert_raises, nottest
from flatten import Flatten, FlattenIterative, FlattenRecursive, FlattenIterativeStack, FlattenGenerator

class BaseTestFlatten:
    def __init__(self, flatten_class, wrapper_function=lambda x: x):
        self.flatten_class = flatten_class
        self.wrapper_function = wrapper_function

    def setup(self):
        self.flatten_function = Flatten(strategy=self.flatten_class).flatten

    def teardown(self):
        self.flatten_function = None

    @raises(TypeError)
    def test_raise_exception_on_none(self):
        self.wrapper_function(self.flatten_function(None))

    def assert_raises_with_message(self, error, message, fn, *args, **kwargs):
        # assert_raises doesn't have an option to print a message
        try:
            assert_raises(error, fn, *args, **kwargs)
        except AssertionError as ex:
            ex.args = ex.args + (message,)
            raise ex

    def test_raise_exception_on_invalid_types(self):
        invalid_types = [1, "", ["must fail", "plz"],
                         [[["must"], "fail"], "plz"],
                         [[], 1, 's', [4]],
                         {}, False]

        for invalid_type in invalid_types:
            self.assert_raises_with_message(TypeError,
                                            "Testing for argument: '{}'".format(invalid_type),
                                            lambda x: self.wrapper_function(self.flatten_function(x)),
                                            invalid_type)

    @nottest
    def test_example(self, input_array, expected_array, msg=""):
        assert_equals(
            self.wrapper_function(self.flatten_function(input_array)),
            expected_array, msg)

    def test_flat_empty_list(self):
        self.test_example([], [])

    def test_non_flat_empty_list(self):
        self.test_example([[[[]]]], [])

    def test_flat_list_one_element(self):
        self.test_example([1], [1])

    def test_flat_list_multiple_elements(self):
        self.test_example([1, 2, 3], [1, 2, 3])

    def test_non_flat_list_one_element(self):
        self.test_example([[1]], [1])

    def test_non_flat_list_one_element_multiple_layers(self):
        self.test_example([[[[[[[[[[[[[[[[[[[[1]]]]]]]]]]]]]]]]]]]], [1])

    def test_non_flat_list_multiple_elements(self):
        self.test_example([1, [[2], 3, [5]]], [1, 2, 3, 5])

    def test_argument_array_not_modified(self):
        initial_array = [1, [2, [3]], [[[4]]]]
        argument_array = [1, [2, [3]], [[[4]]]]

        self.wrapper_function(self.flatten_function(argument_array))

        assert_equals(argument_array, initial_array)

    def test_default_example(self):
        self.test_example([[1, 2, [3]], 4], [1, 2, 3, 4])

class TestFlattenRecursive(BaseTestFlatten):
    def __init__(self):
        super().__init__(FlattenRecursive)

    @classmethod
    def setup_class(cls):
        print("*** Testing Recursive Flatten ***")

class TestFlattenGenerator(BaseTestFlatten):
    def __init__(self):
        super().__init__(FlattenGenerator, list)

    @classmethod
    def setup_class(cls):
        print("*** Testing Recursive Generator Flatten ***")

class TestFlattenIterative(BaseTestFlatten):
    def __init__(self):
        super().__init__(FlattenIterative)

    @classmethod
    def setup_class(cls):
        print("*** Testing Iterative Flatten ***")

class TestFlattenIterativeStack(BaseTestFlatten):
    def __init__(self):
        super().__init__(FlattenIterativeStack)

    @classmethod
    def setup_class(cls):
        print("*** Testing Iterative Flatten with Stack ***")
