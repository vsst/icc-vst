
class Flatten:
    def __init__(self, strategy=None):
        self.strategy = None
        if not strategy is None:
            self.strategy = strategy()


    def flatten(self, array):
        if not self.strategy is None:
            return self.strategy.flatten(array)

        raise Exception("strategy is not set")


class FlattenIterative(Flatten):
    def flatten(self, array):
        if not isinstance(array, list):
            raise TypeError("'{}' must be of type list".format(array))

        if len(array) == 0:
            return []

        flattened_array = list(array)

        for i, _ in enumerate(flattened_array):
            while i < len(flattened_array):
                element = flattened_array[i]
                if isinstance(element, list):
                    flattened_array[i:i+1] = element
                elif not isinstance(element, int):
                    raise TypeError("'{}' is not of type int or list".format(element))
                else:
                    break;
        return flattened_array

class FlattenIterativeStack(Flatten):
    def flatten(self, array):
        if not isinstance(array, list):
            raise TypeError("'{}' must be of type list".format(array))

        if len(array) == 0:
            return []

        stack = list(array)
        flattened_array = []

        while len(stack) > 0:
            head = stack.pop()

            if isinstance(head, list):
                for element in head:
                    stack.append(element)
            elif isinstance(head, int):
                flattened_array.append(head)
            else:
                raise TypeError("'{}' is not of type int or list".format(head))

        flattened_array.reverse()

        return flattened_array

class FlattenRecursive(Flatten):
    def flatten(self, array):
        if not isinstance(array, list):
            raise TypeError("'{}' must be of type list".format(array))

        if len(array) == 0:
            return []

        (head, *tail) = array
        flattened_tail = self.flatten(tail)

        if isinstance(head, list):
            return self.flatten(head) + flattened_tail
        elif not isinstance(head, int):
            raise TypeError("'{}' is not of type int or list".format(head))

        return [head] + flattened_tail

class FlattenGenerator(Flatten):
    def flatten(self, array):
        if not isinstance(array, list):
            raise TypeError("'{}' must be of type list".format(array))

        if len(array) == 0:
            return []

        for element in array:
            if isinstance(element, list):
                for flat in self.flatten(element):
                    yield flat
            elif isinstance(element, int):
                yield element
            else:
                raise TypeError("'{}' is not of type int or list".format(element))

if __name__ == "__main__":
    example = [[1, 2, [3]], 4]
    print("FlattenIterative:\t{} -> {}".format(example, Flatten(strategy=FlattenIterative).flatten(example)))
    print("FlattenIterativeStack:\t{} -> {}".format(example, Flatten(strategy=FlattenIterativeStack).flatten(example)))
    print("FlattenRecursive:\t{} -> {}".format(example, Flatten(strategy=FlattenRecursive).flatten(example)))
    print("FlattenGenerator:\t{} -> {}".format(example, list(Flatten(strategy=FlattenGenerator).flatten(example))))
