#!/usr/bin/env bash


echo -ne "Flatten Iterative:\t\t"
python -m timeit "from flatten import FlattenIterative; FlattenIterative().flatten([1,[2,[3,[4,[5,[6,[7,[8,[9,[10]]]]]]]]]])"

echo -ne "Flatten Iterative with Stack:\t"
python -m timeit "from flatten import FlattenIterativeStack; FlattenIterativeStack().flatten([1,[2,[3,[4,[5,[6,[7,[8,[9,[10]]]]]]]]]])"

echo -ne "Flatten Recursive:\t\t"
python -m timeit "from flatten import FlattenRecursive; FlattenRecursive().flatten([1,[2,[3,[4,[5,[6,[7,[8,[9,[10]]]]]]]]]])"

echo -ne "Flatten Generator:\t\t"
python -m timeit "from flatten import FlattenGenerator; FlattenGenerator().flatten([1,[2,[3,[4,[5,[6,[7,[8,[9,[10]]]]]]]]]])"

echo -ne "Flatten list(Generator):\t"
python -m timeit "from flatten import FlattenGenerator; list(FlattenGenerator().flatten([1,[2,[3,[4,[5,[6,[7,[8,[9,[10]]]]]]]]]]))"
